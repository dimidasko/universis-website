---
title: "Development"
hero:
  title: "Ανάπτυξη Λογισμικού"
  background_image: "../images/universis-dev-coding.svg"
content_blocks:
  - _bookshop_name: "start"
    preheading: "Open Source Agile Development"
    heading: "Διαφανείς διαδικασίες ανάπτυξης με τον κώδικα, τα αιτήματα βελτίωσης και τις διόρθωσεις όλα δημόσια διαθέσιμα."
    text: "Με ευέλικτη σχεδίαση και διαδοχικούς κύκλους ανάπτυξης σε sprints δύο εβδομάδων χτίζουμε σταδιακά τη νέα λειτουργικότητα."
    image_path: "../images/universis-scrum-board.svg"
    button:
      text: "universis.io"
      url: "https://universis.io"
  - _bookshop_name: "counter_dark"
    numbers:
      - icon: "ti-check"
        number: 80
        suffix: "+"
        text: "Ενεργά Projects που απαρτίζουν το οικοσύστημα των εφαρμογών"
      - icon: "ti-flag"
        number: 1000
        suffix: "+"
        text: "Merge Requests τον τελευταίο χρόνο"
      - icon: "ti-layers"
        number: 25
        suffix: ""
        text: "Developers που συνεισφέρουν κώδικα"
      - icon: "ti-medall"
        number: 35
        suffix: "+"
        text: "Επαναχρησιμοποιήσιμα modules κώδικα σε npmjs projects"
  - _bookshop_name: "team"
    preheading: "Γνωρίστε το οικοσύστημα"
    heading: "Τα μέρη της λύσης UniverSIS"
    people:
      - name: "students"
        image: "../images/1-student-dashboard.jpg"
        role: "Εφαρμογή Φοιτητή"
        gitlab: "universis/universis-students"
      - name: "teachers"
        image: "../images/1-teacher-dashboard.jpg"
        role: "Εφαρμογή Διδάσκοντα"
        gitlab: "universis/universis-teachers"
      - name: "registrar"
        image: "../images/1-registrar-dashboard.jpg"
        role: "Εφαρμογή Γραμματείας"
        gitlab: "universis/universis"
      - name: "universis API"
        image: "../images/universis-api.png"
        role: "ORM enabled backend"
        gitlab: "universis/universis-api"
      - name: "OAUTH server"
        image: "../images/keycloak.png"
        role: "Identity and Access Management Server"
      - name: "Database Server"
        image: "../images/databases.png"
        role: "Βάση δεδομένων"
---
