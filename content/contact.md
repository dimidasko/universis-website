---
title: "Επικοινωνία"
hero:
  title: "Ρωτήστε μας"
  background_image: "../images/universis-interview.jpg"
content_blocks:
  - _bookshop_name: "contact_form"
    heading: "Στη διάθεσή σας για οποιοδήποτε ερώτημα"
    form_heading: "Φόρμα επικοινωνίας"
    address1: "ΚΗΔ, Αριστοτέλειο Πανεπιστήμιο Θεσσαλονίκης"
    address2: "ΚΔΔ, Δημοκρίτειο Πανεπιστήμιο Θράκης"
    email: info@universis.gr 
    linkedin: universis
---
