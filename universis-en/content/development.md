---
title: "Code"
hero:
  title: "Software Development"
  background_image: "../../images/universis-dev-coding.svg"
content_blocks:
  - _bookshop_name: "start"
    preheading: "Open Source Agile Development"
    heading: "Open development procedures with public code repositories, bug and feature request issues and code merge requests."
    text: "Agile development with flexible design following consecutive development cycles in two-week sprints building gradually new functionality."
    image_path: "../images/universis-scrum-board.svg"
    button:
      text: "universis.io"
      url: "https://universis.io"
  - _bookshop_name: "counter_dark"
    numbers:
      - icon: "ti-check"
        number: 80
        suffix: "+"
        text: "Active Projects comprising our ecosystem"
      - icon: "ti-flag"
        number: 1000
        suffix: "+"
        text: "Merge Requests during the last year"
      - icon: "ti-layers"
        number: 25
        suffix: ""
        text: "Contributing Developers"
      - icon: "ti-medall"
        number: 35
        suffix: "+"
        text: "Reusable npmjs modules"
  - _bookshop_name: "team"
    preheading: "Our ecosystem"
    heading: "The UniverSIS solution"
    people:
      - name: "students"
        image: "../images/1-student-dashboard.jpg"
        role: "The Student application"
        gitlab: "universis/universis-students"
      - name: "teachers"
        image: "../images/1-teacher-dashboard.jpg"
        role: "The Instructor application"
        gitlab: "universis/universis-teachers"
      - name: "registrar"
        image: "../images/1-registrar-dashboard.jpg"
        role: "The Registrar application"
        gitlab: "universis/universis"
      - name: "universis API"
        image: "../images/universis-api.png"
        role: "ORM enabled backend"
        gitlab: "universis/universis-api"
      - name: "OAUTH server"
        image: "../images/keycloak.png"
        role: "Identity and Access Management Server"
      - name: "Database Server"
        image: "../images/databases.png"
        role: "Data layer"
---
