---
title: "Contact"
hero:
  title: "Ask us"
  background_image: "../../images/universis-interview.jpg"
content_blocks:
  - _bookshop_name: "contact_form"
    heading: "For info"
    form_heading: "Contact Form"
    address1: "IT Center, Aristotle University of Thessaloniki"
    address2: "IT Center, Democritus University of Thrace"
    email: info@universis.gr 
    linkedin: universis
---
