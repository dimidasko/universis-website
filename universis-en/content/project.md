---
title: "Project"
hero:
  title: "Project"
  background_image: "../../images/universis-collab3.png"
content_blocks:
  - _bookshop_name: "start"
    preheading: "An ambitious goal"
    heading: "Creation of an open Student Information System solution"
    text: "The information system operating at the core of every Academic Isntitution was our goal since 2015, the year we started our mission to optimize the services offered to all students and instructors. Until an even bigger goal was set in 2018, to set the foundations for a platform maintained and extended by the community it serves"
    image_path: "../../images/universis-dashboard.jpg"
#    button:
#      text: "UniverSIS - Εισαγωγή"
#      url: "https://universis.gitlab.io/universis-tech-brochure/intro/"
  - _bookshop_name: "presentations"
    preheading: "Evolution of our project"
    heading: "Presentations"
    sections:
      - title: "November 2018"
#        image_path: "../presentations/2018-nov-grnet.pdf"
        icon: "ti-calendar"
        content: "Symposium for the 20 years of the Greek NREN - GRNET"
      - title: "June 2020"
#        image_path: "../presentations/2020-jul-gunet.pdf"
        icon: "ti-calendar"
        content: "Tech event - Greek Universities Network GUNET"
      - title: "July 2021"
#        image_path: "../presentations/2021-jul-gunet.pdf"
        icon: "ti-calendar"
        content: "Tech event - Greek Universities Network GUNET"
      - title: "July 2022"
#        image_path: "../presentations/2022-jul-gunet.pdf"
        icon: "ti-calendar"
        content: "Tech event - Greek Universities Network GUNET"
  - _bookshop_name: "features"
    heading: "Characteristics"
    sections:
      - title: "Optimized User Experience"
        icon: "ti-mobile"
        url: "https://universis.gitlab.io/universis-tech-brochure/extras/#accessibility"
        content: "Adopting a user-centered approach, design based on user experience, research and evaluation"
      - title: "Modern Architecture"
        icon: "ti-rocket"
        url: "https://universis.gitlab.io/universis-tech-brochure/technology/#architecture"
        content: "Based on state-of-the-art technology stack, responsive browser side applications, backend with ORM-enabled API, database agnostic, cross-platform, scalable"
      - title: "Secure"
        icon: "ti-lock"
        url: "https://universis.gitlab.io/universis-tech-brochure/technology/#security"
        content: "Designed from the ground up with security in mind for the daily procedures, as well as the long term retention of data with verification and traceablity capabilities"
      - title: "Flexible Installation"
        icon: "ti-settings"
        content: "Allows integration with existing infrastructure and services, while supporting parallel usage with legacy systems for gradual migration"
      - title: "Compatibility"
        icon: "ti-server"
        content: "Platform fully operable on all devices and environments, web and mobile"
      - title: "Engaged Community"
        icon: "ti-user"
        content: "Based on cooperative principles, we seek the participation of all Academic Institutions to maximize the usability of our solution"
---
